[![pipeline status](https://gitlab.com/unveiled_adri/jekyll_project/badges/master/pipeline.svg)](https://gitlab.com/unveiled_adri/jekyll_project/commits/master)

# Proyecto jekyll

En este repositorio se encuentran los archivos de configuración de jekyll (no están los html de _site)    
Mediante el archivo .gitlab-ci.yml, se desplegará la web automáticamente después de cada push al repositorio  

[Entra a la web desde aquí](https://unveiled_adri.gitlab.io/jekyll_project/)
