---
layout: page
title: Océano
permalink: /Oceano/
---

## También conocido como el oceano mundial
![Imagen de oceano](../oceano.jpg)      
Océano, titán del mar en la mitología griega durante la era pre-olímpica, quien fue hijo de Urano y Gea, era caracterizado por cubrir de inmensos mares a las tierras de todo el planeta y permitir la vida de todos los seres que allí habitan, por lo que el horizonte culminaba en él.

![Oceano cola escamada](https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Pergamon_Museum_Berlin_2007034.jpg/800px-Pergamon_Museum_Berlin_2007034.jpg)
*Océano, a la derecha, con cola escamada, en la Gigantomaquia del Altar de Pérgamo*

### Consortes y descendencia
* Gea
	* Creúsa
	* Triptólemo (según Apolodoro)
* Theia, hija de Memnón
	* Cercopes
* Tetis
	* Oceánidas (dioses fluviales)
	* Oceánides (ninfas marinas)


<p>Por otra parte, los romanos hicieron esculturas en fuentes de agua pública y estas representaban los dioses de los mares y sus criaturas, tal es el caso de la Fontana di Trevi.</p>

### Fuentes externas
---  
[Enlace 1](http://www.maicar.com/GML/Oceanus.html)  
[Enlace 2](http://theoi.com/Titan/TitanOkeanos.html)
