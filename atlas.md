---
layout: page
title: Atlas
permalink: /Atlas/
---

## También conocido como el portador
![Imagen de atlas](../atlas.jpg)

En la mitología griega, Atlante, Atlas o Atlantis (en griego antiguo Ἄτλας, ‘el portador’, de τλάω tláô, ‘portar’, ‘soportar’) 
era un joven titán al que Zeus condenó a cargar sobre sus hombros al cielo (es decir, Urano). Era hijo de Jápeto y la ninfa Clímene 
(en otras versiones, de Asia) y hermano de Prometeo, Epimeteo y Menecio. Higino, sin embargo, lo hace hijo de Gea y Éter o Urano, aunque el 
texto del Prefacio, donde hace esta afirmación, está algo corrupto. Fue el padre de las Hespérides (con Hesperis), Mera, las Híades, Calipso 
y las Pléyades. 

![Escultura moderna](https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Objectivist1.jpg/800px-Objectivist1.jpg)
*Escultura de atlas moderna*

Otros usos para el nombre Atlas:
* Bioshock, Atlas es el apodo del antagonista en el popular videojuego del año 2007, Frank Fontaine.
* Atlas Shrugged es parte del libro creado por la filósofa Ayn Rand
* Atlas, Rise! es una canción de la banda de thrash metal Metallica.

<p>Atlas acaudilló a los Titanes en la Titanomaquia o guerra contra los olímpicos. 
Cuando los Titanes fueron derrotados , Zeus condenó a Atlas a cargar con el cielo (a Urano), 
muy cerca del jardín de las Hespérides. </p>

### Fuentes externas
---  
[Enlace 1](http://www.maicar.com/GML/Atlas.html)  
[Enlace 2](https://meta.wikimedia.org/wiki/s:es:Las_metamorfosis:_Libro_IV#Perseo_y_Atlas_.28604_-_662.29)
