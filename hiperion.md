---
layout: page
title: Hiperión
permalink: /Hiperion/
---

## También conocido como el que camina en las alturas
![Imagen de hiperion](../hiperion.jpg)

Según la mitología griega Hiperión (que significa el que camina en las alturas) es uno de los titanes hijos de Urano (El Cielo) y Gea (La Tierra) considerado como el dios de la observación junto con su hermana Tea quien es la diosa de la vista, con quien según Hesíodo se casó y tuvo tres hijos quienes eran Helios (el Sol), Selene (la Luna), y Eos (la Aurora).

![Dibujo hiperion](https://www.mitologia.info/wp-content/uploads/2017/05/Hiperion.jpg)   

 
*Dibujo de hiperón*

### Curiosidades
* Algunos dicen que Hiperión es el Dios de la Luz, y Tea la Diosa del fuego, y que por esa unión nació Helios, Dios del Sol. 
* Murió en la titanomaquía(guerra de los titanes) 
* En su honor, una de las lunas medianas heladas de Saturno se llama Hiperión y sus cráteres llevan nombres de divinidades astrales: Helios, (su hijo), Jarilo (dios eslavo del sol), Meri (dios sol de los bororo de Brasil) y Bahloo (dios australiano de la luna). 

<p>De esta manera, Hiperión al viajar sobre los cielos permitió relacionar a los hombres los ciclos de la vida con las estaciones; lo que hace que se vuelva el titán de la sabiduría y la comprensión.</p>

### Fuentes externas
---  
[Enlace 1](http://theoi.com/Titan/TitanHyperion.html)  
[Enlace 2](https://es.wikipedia.org/wiki/Titanes)
